package com.retouch.kaho.folders;

import java.util.*;

public class KahoService {
	
	private List<Folder> folders = new ArrayList<>(Arrays.asList(new Folder("1","Kahraman"),new Folder("2","Kahraman")));
	
	public List<Folder> getAllFolders(){
		
		return folders;
		
	}
	public Folder getFolder(String id) {
		return folders.stream().filter(t -> t.getId().equals(id)).findFirst().get();		
	}

}
