package com.retouch.kaho.folders;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class FolderController {
//	@Autowired
//	private KahoService kahoService;
	
	@GetMapping(path = "/folders/{id}")
	public @ResponseBody Folder getFolderById(@PathVariable String id) {
		Folder f=new Folder();
		f.setId(id);
		f.setName("Kahraman");
		return f;
	//	return kahoService.getFolder(id);
				
	}
	
}
