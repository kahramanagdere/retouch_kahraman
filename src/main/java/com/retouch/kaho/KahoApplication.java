package com.retouch.kaho;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KahoApplication {

	public static void main(String[] args) {
		SpringApplication.run(KahoApplication.class, args);
	}

}
